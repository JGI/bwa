FROM debian:jessie
LABEL Maintainer Shijie Yao, syao@lbl.gov

# make alias active
RUN sed -i 's/# alias/alias/' ~/.bashrc

# install the needed programs
#ENV PACKAGES wget tar
#RUN apt-get update && apt-get install --yes ${PACKAGES}
RUN apt-get update 

# create download and move into it
WORKDIR /workdir

# download and install bwa 
RUN apt-get install git --yes	&& \
    apt-get install build-essential  --yes	&& \
    git clone https://github.com/lh3/bwa.git	&& \
    apt-get install zlib1g-dev --yes	&& \
    cd bwa; make; mv bwa /usr/bin/.	&& \
    apt-get remove --purge build-essential --yes && \
    apt-get remove --purge git --yes && \
    apt-get remove --purge zlib1g-dev --yes 


# to run:
#docker run --rm bwa bwa 

